#!/bin/sh

set -e

CANAME=ca
CAKEY=${CANAME}.key
CA=${CANAME}.crt
CADAYS=365

DEFAULTDAYS=30

show_help()
{
  echo "sslutil.sh genca - Generates CA key and certificate.
sslutil.sh gencsr [opts] <name> - Generates certificate request.
  "
}

show_csr_help()
{
  echo "sslutil.sh gencsr [opts] <name> - Generates certificate request.
Arguments:
name  - The name of the output certificate request file.
Options:
s - Indicates that the certificate is to be used to authenticate as server.
c - Indicates that the certificate is to be used to authenticate as client.
i - Adds an IP address to the certificate. Can be repeated.
d - Adds a domain name to the certificate. Can be repeated.
  "
}

gen_key()
{
  if [ $# -lt 1 ]; then
    echo "Missing key name"
    return 1
  fi
  openssl genrsa -out ${1}.key
}

gen_ca()
{
  if [ ! -d certs ]; then
    mkdir certs
  fi

  if [ ! -f index.txt ]; then
    touch index.txt
  fi

  if [ ! -f serial ]; then
    echo '01' > serial
  fi

  if [ ! -f "${CAKEY}" ]; then
    gen_key ${CANAME}
  fi

  # Generate the self-signed CA certificate
  openssl req -x509 -sha256 -nodes -days +${CADAYS} \
    -config ./openssl.cnf \
    -key ${CAKEY} -out ${CA} \
    -addext keyUsage=critical,digitalSignature,keyCertSign,cRLSign
}

gen_csr()
{
  EXT_KEY_USAGE=
  IP=
  DNS=

  # Process options
  while getopts sci:d: opt ; do
    case $opt in
      s)
        EXT_KEY_USAGE+=",serverAuth"
        ;;
      c)
        EXT_KEY_USAGE+=",clientAuth"
        ;;
      i)
        IP+=",IP:${OPTARG}"
        ;;
      d)
        DNS+=",DNS:${OPTARG}"
        ;;
      \?)
        echo "Invalid option ${OPTARG}"
        show_csr_help
        return 1
        ;;
      :)
        echo "Option ${OPTARG} requires an argument"
        show_csr_help
        return 1
        ;;
    esac
  done
  shift $((OPTIND-1))

  # Process CSR extensions
  if [ ! -z "${EXT_KEY_USAGE}" ]; then
    EXT_KEY_USAGE="i\
extendedKeyUsage=critical,${EXT_KEY_USAGE##,}"
  fi
  SAN=
  if [ ! -z "${IP}" ]; then
    SAN+=",${IP##,}"
  fi
  if [ ! -z ${DNS} ]; then
    echo "DNS not empty"
    SAN+=",${DNS##,}"
  fi
  if [ ! -z "${SAN}" ]; then
    SAN="i\
subjectAltName=${SAN##,}"
  fi

  # Process CSR file name
  if [ $# -eq 0 ]; then
    echo "Missing CSR name"
    show_csr_help
    return 1
  fi
  NAME=${1}

  # Generate private key if it does not exist
  if [ ! -f ${NAME}.key ]; then
    gen_key ${NAME}
  fi

  sed -e "/^\[\s*v3_req\s*\]$/,/^\[/ {
    /^\[\s*v3_req\s*\]$/n
    /^\[/ {
      ${EXT_KEY_USAGE}
      ${SAN}
    }
  }" openssl.cnf > ${NAME}.conf

  # Generate the CSR
  openssl req -new -key ${NAME}.key -out ${NAME}.csr \
    -config ${NAME}.conf

  rm -f ${NAME}.conf
}

sign_req()
{
  if [ $# -lt 1 ]; then
    echo "Missing certificate name"
    return 1
  fi
  NAME=${1}

  if [ ! -f ${NAME}.csr ]; then
    echo "Missing request file ${NAME}.csr"
    return 1
  fi

  # Make sure CA files exist
  if [ ! -f ${CAKEY} -o ! -f ${CA} ]; then
    echo "Missing CA. Please generate using 'sslutil.sh genca' first!"
    return 1
  fi

  openssl ca -config openssl.cnf -in ${NAME}.csr -out ${NAME}.crt \
    -create_serial -days +${DEFAULTDAYS}

  # SERIAL=1

  # openssl x509 -sha256 -req \
  #   -in ${NAME}.csr \
  #   -out ${NAME}.crt \
  #   -CA ${CA} \
  #   -CAkey ${CAKEY} \
  #   -days 30 \
  #   -set_serial ${SERIAL} \
  #   -extensions "basicConstraints=critical,CA:false" \
  #   -extensions "subjectKeyIdentifier=hash" \
  #   -extensions "authorityKeyIdentifier=keyid,issuer"
}

case $1 in
  genca)
    gen_ca
    ;;
  gencsr)
    gen_csr ${@:2}
    ;;
  sign)
    sign_req ${@:2}
    ;;
  *)
    echo "Invalid command"
    show_help
    exit 1
    ;;
esac
