package ssh

import (
	"fmt"
	"path/filepath"

	"github.com/urfave/cli/v2"
	"gitlab.com/zixian92/keymaster/util/ssh"
)

// GenerateCA generates an SSH CA private and public keys.
func GenerateCA(c *cli.Context) error {
	privateKey, err := ssh.GenKey()
	if err != nil {
		return fmt.Errorf("failed to generate SSH CA private key: %v", err)
	}

	publicKey, err := ssh.GetPublicKey(&privateKey.PublicKey)
	if err != nil {
		return fmt.Errorf("failed to get SSH CA public key: %v", err)
	}

	keyName := c.String("name")

	if err := ssh.SaveKey(privateKey, filepath.Join(sshFolder, keyName)); err != nil {
		return err
	}

	if err := ssh.SavePublicKey(publicKey, filepath.Join(sshFolder, keyName+".pub")); err != nil {
		return err
	}

	return nil
}
