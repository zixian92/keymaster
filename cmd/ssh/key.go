package ssh

import (
	"fmt"
	"path/filepath"

	"github.com/urfave/cli/v2"
	"gitlab.com/zixian92/keymaster/util"
	"gitlab.com/zixian92/keymaster/util/ssh"
)

// GenerateKeys generates a private and public key pair.
func GenerateKeys(c *cli.Context) error {
	// Check that user supplies name argument
	name := c.Args().Get(0)
	if name == "" {
		return fmt.Errorf("missing key name")
	}

	// Check that the output file paths are not already taken
	if util.FileExists(filepath.Join(sshFolder, name)) {
		return fmt.Errorf("%s already exists", filepath.Join(sshFolder, name))
	}
	if util.FileExists(filepath.Join(sshFolder, name+".pub")) {
		return fmt.Errorf("%s already exists", filepath.Join(sshFolder, name+".pub"))
	}

	privateKey, err := ssh.GenKey()
	if err != nil {
		return fmt.Errorf("failed to generate SSH private key: %v", err)
	}

	publicKey, err := ssh.GetPublicKey(&privateKey.PublicKey)
	if err != nil {
		return fmt.Errorf("failed to get SSH public key: %v", err)
	}

	if err := ssh.SaveKey(privateKey, filepath.Join(sshFolder, name)); err != nil {
		return err
	}

	if err := ssh.SavePublicKey(publicKey, filepath.Join(sshFolder, name+".pub")); err != nil {
		return err
	}

	return nil
}
