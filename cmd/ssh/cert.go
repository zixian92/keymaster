package ssh

import (
	"fmt"
	"path/filepath"

	"github.com/urfave/cli/v2"
	"golang.org/x/crypto/ssh"

	sshutil "gitlab.com/zixian92/keymaster/util/ssh"
)

// SignCertificate signs a public key using the given CA.
// Returns non-nil error on failure.
func SignCertificate(c *cli.Context) error {
	name := c.Args().Get(0)
	if name == "" {
		return fmt.Errorf("missing key name")
	}

	// Get essential information from flags
	ca := c.String("ca")
	isHost := c.Bool("host")
	numDays := c.Uint("days")
	principals := c.StringSlice("principal")

	// Craft the certificate template
	cert := &ssh.Certificate{
		KeyId:           name,
		ValidPrincipals: principals,
	}
	if isHost {
		cert.CertType = ssh.HostCert
	} else {
		cert.CertType = ssh.UserCert
	}

	// Sign the certificate
	cert, err := sshutil.SignCertificate(
		cert,
		filepath.Join(sshFolder, ca),
		filepath.Join(sshFolder, name+".pub"),
		numDays)
	if err != nil {
		return err
	}

	// Save the certificate to file
	return sshutil.SavePublicKey(cert, filepath.Join(sshFolder, name+"-cert.pub"))
}
