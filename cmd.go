package main

import (
	"fmt"
	"log"
	"os"

	"github.com/urfave/cli/v2"

	"gitlab.com/zixian92/keymaster/cmd/ssh"
)

var Version string
var BuildDate string

func main() {
	app := &cli.App{
		Name:  "keymaster",
		Usage: "Manage SSL and SSH keys/certificates",
		Commands: []*cli.Command{
			{
				Name:  "ssl",
				Usage: "Manage SSL keys/certificates",
				Action: func(c *cli.Context) error {
					return fmt.Errorf("not implemented yet")
				},
			},
			{
				Name:  "ssh",
				Usage: "Manage SSH keys/certificates. All keys and certificates are stored in ssh subfolder.",
				Subcommands: []*cli.Command{
					{
						Name:  "genca",
						Usage: "Generates SSH CA private and public keys",
						Flags: []cli.Flag{
							&cli.StringFlag{
								Name:    "name",
								Aliases: []string{"n"},
								Usage:   "Name of the CA file",
								Value:   "ca",
							},
						},
						Action: ssh.GenerateCA,
					},
					{
						Name:   "genkey",
						Usage:  "Generates a pair of SSH private and public keys",
						Action: ssh.GenerateKeys,
					},
					{
						Name:  "sign",
						Usage: "Signs a public key using the given CA key",
						Flags: []cli.Flag{
							&cli.StringFlag{
								Name:  "ca",
								Usage: "The name of the CA to use",
								Value: "ca",
							},
							&cli.UintFlag{
								Name:    "days",
								Aliases: []string{"d"},
								Usage:   "How many days to sign the certificate for",
								Value:   30,
							},
							&cli.BoolFlag{
								Name:  "host",
								Usage: "Whether to sign a host certificate",
								Value: false,
							},
							&cli.StringSliceFlag{
								Name:     "principal",
								Aliases:  []string{"n"},
								Usage:    "Principals to add to the certificate",
								Required: true,
							},
						},
						Action: ssh.SignCertificate,
					},
				},
			},
			{
				Name:  "version",
				Usage: "Prints the current version of this program",
				Action: func(c *cli.Context) error {
					fmt.Printf("Version %s built on %s", Version, BuildDate)
					return nil
				},
			},
			{
				Name:  "upgrade",
				Usage: "Upgrades this program to the latest version",
				Action: func(c *cli.Context) error {
					return fmt.Errorf("not yet implemented")
				},
			},
		},
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
