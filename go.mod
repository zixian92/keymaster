module gitlab.com/zixian92/keymaster

go 1.14

require (
	github.com/urfave/cli/v2 v2.2.0
	golang.org/x/crypto v0.0.0-20200406173513-056763e48d71
)
