package util

import (
	"os"
)

// FileExists checks whether file/folder exists
// at the given path.
func FileExists(fPath string) bool {
	_, err := os.Stat(fPath)
	return !os.IsNotExist(err)
}
