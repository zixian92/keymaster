package ssh

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"

	"golang.org/x/crypto/ssh"
)

// GenKey generates a new SSH private key
// using 2048-bit RSA algorithm.
// Returns non-nil error on failure.
func GenKey() (*rsa.PrivateKey, error) {
	key, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		return nil, err
	}

	if err := key.Validate(); err != nil {
		return nil, err
	}

	return key, nil
}

// GetPublicKey converts the given RSA public key into
// SSH public key.
// Returns non-nil error on failure.
func GetPublicKey(key *rsa.PublicKey) (ssh.PublicKey, error) {
	return ssh.NewPublicKey(key)
}

// LoadKey loads the given SSH private key from file.
// Returns non-nil error on failure.
func LoadKey(fPath string) (*rsa.PrivateKey, error) {
	pemBytes, err := ioutil.ReadFile(fPath)
	if err != nil {
		return nil, fmt.Errorf("failed to read private key %s: %v", fPath, err)
	}

	blk, _ := pem.Decode(pemBytes)

	privateKey, err := x509.ParsePKCS1PrivateKey(blk.Bytes)
	if err != nil {
		return nil, fmt.Errorf("failed to parse private key %s: %v", fPath, err)
	}

	return privateKey, nil
}

// LoadKeySigner loads the given SSH private key as a signer
// which can be used to sign/encrypt stuff.
// Returns non-nil error on failure.
func LoadKeySigner(fPath string) (ssh.Signer, error) {
	key, err := LoadKey(fPath)
	if err != nil {
		return nil, err
	}

	signer, err := ssh.NewSignerFromKey(key)
	if err != nil {
		return nil, fmt.Errorf("failed to convert private key %s to signer: %v", fPath, err)
	}

	return signer, nil
}

// LoadCertificateSigner loads the given private key and public key certificate
// from file.
// The resulting signer is used for establishing SSH connection.
// Returns non-nil error on failure.
func LoadCertificateSigner(keyPath, certPath string) (ssh.Signer, error) {
	key, err := LoadKeySigner(keyPath)
	if err != nil {
		return nil, err
	}
	cert, err := LoadCertificate(certPath)
	if err != nil {
		return nil, err
	}

	signer, err := ssh.NewCertSigner(cert, key)
	if err != nil {
		return nil, fmt.Errorf("key does not match certificate: %v", err)
	}

	return signer, nil
}

// SaveKey saves the given SSH private key into the given file path.
// Returns non-nil error on failure.
func SaveKey(key *rsa.PrivateKey, fPath string) error {
	derBytes := x509.MarshalPKCS1PrivateKey(key)

	blk := &pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: derBytes,
	}

	if err := os.MkdirAll(filepath.Dir(fPath), 0755); err != nil {
		return fmt.Errorf("failed to create folder %s for private key: %v", filepath.Dir(fPath), err)
	}

	f, err := os.OpenFile(fPath, os.O_CREATE|os.O_WRONLY, 0400)
	if err != nil {
		return fmt.Errorf("failed to open private key file %s for writing: %v", fPath, err)
	}
	defer f.Close()

	return pem.Encode(f, blk)
}

// LoadPublicKey loads the public key from file.
// Returns non-nil error on failure.
func LoadPublicKey(fPath string) (ssh.PublicKey, error) {
	fBytes, err := ioutil.ReadFile(fPath)
	if err != nil {
		return nil, fmt.Errorf("failed to read public key %s: %v", fPath, err)
	}

	publicKey, _, _, _, err := ssh.ParseAuthorizedKey(fBytes)
	if err != nil {
		return nil, fmt.Errorf("failed to parse public key %s: %v", fPath, err)
	}

	return publicKey, nil
}

// SavePublicKey saves the given SSH public key to the given file path.
// Returns non-nil error on failure.
func SavePublicKey(key ssh.PublicKey, fPath string) error {
	keyBytes := ssh.MarshalAuthorizedKey(key)

	if err := os.MkdirAll(filepath.Dir(fPath), 0755); err != nil {
		return fmt.Errorf("failed to create folder %s for public key: %v", filepath.Dir(fPath), err)
	}

	if err := ioutil.WriteFile(fPath, keyBytes, 0444); err != nil {
		return fmt.Errorf("failed to write public key to file %s: %v", fPath, err)
	}

	return nil
}
