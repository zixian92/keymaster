package ssh

import (
	"crypto/rand"
	"fmt"
	"io"
	"io/ioutil"
	"path/filepath"
	"strconv"
	"time"

	"gitlab.com/zixian92/keymaster/util"
	"golang.org/x/crypto/ssh"
)

// SignCertificate signs the given certificate template.
// This is mainly aconvenience method for signing as it handles all the loading
// of CA and public key, setting the serial number etc.
// Returns non-nil error on failure.
func SignCertificate(templ *ssh.Certificate, caKeyFile, pubKeyFile string, days uint) (*ssh.Certificate, error) {
	//Load CA key
	ca, err := LoadKeySigner(caKeyFile)
	if err != nil {
		return nil, fmt.Errorf("failed to load CA key %s: %v", caKeyFile, err)
	}
	algoCA, ok := ca.(ssh.AlgorithmSigner)
	if !ok {
		return nil, fmt.Errorf("unable to sign using rsa-sha2-512")
	}

	// Set public key associated with certificate
	publicKey, err := LoadPublicKey(pubKeyFile)
	if err != nil {
		return nil, err
	}
	templ.Key = publicKey

	// Set SSH session extensions if is user certificate
	if templ.CertType == ssh.UserCert {
		templ.Permissions = ssh.Permissions{
			Extensions: map[string]string{
				"permit-X11-forwarding":   "",
				"permit-agent-forwarding": "",
				"permit-port-forwarding":  "",
				"permit-pty":              "",
				"permit-user-rc":          "",
			},
		}
	}

	// Set serial number of certificate
	caDir := filepath.Dir(caKeyFile)
	caName := filepath.Base(caKeyFile)
	serialName := caName + "_serial"
	serialPath := filepath.Join(caDir, serialName)
	if !util.FileExists(serialPath) {
		if err := ioutil.WriteFile(serialPath, []byte("02"), 0644); err != nil {
			return nil, fmt.Errorf("failed to commit serial number to %s: %v", serialPath, err)
		}
		templ.Serial = uint64(1)
	} else {
		serialBytes, err := ioutil.ReadFile(serialPath)
		if err != nil {
			return nil, fmt.Errorf("failed to read serial from %s: %v", serialPath, err)
		}

		// Get serial
		serial, err := strconv.ParseUint(string(serialBytes), 10, 64)
		if err != nil {
			return nil, fmt.Errorf("failed to parse serial from %s: %v", serialPath, err)
		}

		// Write out updated serial
		if err := ioutil.WriteFile(serialPath, []byte(fmt.Sprintf("%02d", serial+1)), 0644); err != nil {
			return nil, fmt.Errorf("failed to commit updated serial to %s: %v", serialPath, err)
		}

		templ.Serial = serial
	}

	now := time.Now()
	templ.ValidAfter = uint64(now.Unix())
	templ.ValidBefore = uint64(now.Add(time.Duration(days) * 24 * time.Hour).Unix())
	templ.SignatureKey = algoCA.PublicKey()

	// Set nonce
	// Refer to https://github.com/golang/crypto/blob/master/ssh/certs.go#L419
	templ.Nonce = make([]byte, 32)
	if _, err := io.ReadFull(rand.Reader, templ.Nonce); err != nil {
		return nil, fmt.Errorf("failed to set certificate nonce: %v", err)
	}

	// Re-implementing templ.CertSign using different algorithm
	// Refer to https://github.com/golang/crypto/blob/master/ssh/certs.go#L419 and
	// https://github.com/golang/crypto/blob/master/ssh/certs.go#L456
	certCopy := *templ
	certCopy.Signature = nil
	signBytes := certCopy.Marshal()
	sig, err := algoCA.SignWithAlgorithm(rand.Reader, signBytes[:len(signBytes)-4], ssh.SigAlgoRSASHA2512)
	if err != nil {
		return nil, fmt.Errorf("failed to sign certificate: %v", err)
	}
	templ.Signature = sig

	return templ, nil
}

// LoadCertificate loads a public key certificate from file.
// Returns non-nil error on failure.
func LoadCertificate(fPath string) (*ssh.Certificate, error) {
	pubKey, err := LoadPublicKey(fPath)
	if err != nil {
		return nil, err
	}

	return pubKey.(*ssh.Certificate), nil
}
